var _data = [{"title":"event 1", "date":"2014/09/25"},
            {"title":"event 2", "date":"2014/09/26","enddate":"2014/09/29"},
            {"title":"event 3", "date":"2014/09/27"},
            {"title":"event 4", "date":"2014/09/30"}];
            
            //örnek json
            /*var _data = [{"title":"event 1", "date":"2014/09/25" ,"enddate":"2014/09/26"},
                        {"title":"event 2", "date":"2014/09/26","enddate":"2014/09/29"},
                        {"title":"event 3", "date":"2014/09/27"},
                        {"title":"event 4", "date":"2014/09/28","enddate":"2014/09/29"},
                        {"title":"event 5", "date":"2014/09/30"},
            ];*/
            
 if(_data.length>0){
    var json_data = JSON.stringify(_data);
 }
 else{
     alert("Etkinlik yok.");
 }

var months = ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"];

var monthAndYear = document.getElementById("monthAndYear");

var tbl_head = document.getElementById("tbl-head");// tablo başlık kısmının css değişimi
tbl_head.style.backgroundColor= "#c1d4e8";
tbl_head.style.color= "#5B7086";

var h = document.createElement("H3");


showCalendar(json_data);

function showCalendar(data) {    
    //değişken tanımları
    var dates= [];
    var end_dates=[];
    var titles=[];

    var new_dates_data=[];
    var new_titles_data=[];
    var day_ms = 1000*60*60*24;
    var diff;  

    parsed_data = JSON.parse(data); //json parsing

    //parçalanan json dizilere doluyor
    parsed_data.forEach(element => {
        titles.push(element.title);
        dates.push(element.date);
        end_dates.push(element.enddate);
    });

    //enddate'i olan eventlara göre dizi yeniden düzenleniyor.
    for(var m=0;m<dates.length;m++){
        if(end_dates[m]!==undefined){
            var date_diff=[];
            date1 = new Date(dates[m]);
            date2 = new Date(end_dates[m]);

            diff = (date2.getTime() - date1.getTime())/day_ms;
            for(var i=0;i<=diff; i++)
            {
                var xx = date1.getTime()+day_ms*i;
                var yy = new Date(xx);
                
                if(yy.getMonth()+1<10){
                    date_diff.push(yy.getFullYear()+"/0"+(yy.getMonth()+1)+"/"+yy.getDate());
                }
                else{
                    date_diff.push(yy.getFullYear()+"/"+(yy.getMonth()+1)+"/"+yy.getDate());
                }
            }
            for(var j=0;j<date_diff.length;j++){

                if(new_dates_data.indexOf(date_diff[j])<0){
                    new_dates_data.push(date_diff[j]);
                    new_titles_data.push(titles[m]);
                }
                else{
                    var tmp_index = new_dates_data.indexOf(date_diff[j]);
                    var indexx = dates.indexOf(date_diff[0]);
                    var tmp_baslik = titles[indexx];
                    var tmp_title = new_titles_data[tmp_index];
                    new_titles_data[tmp_index]= tmp_title + " / " + tmp_baslik;
                }
            }
        }
        else{
            if(new_dates_data.indexOf(dates[m])<0){
                new_dates_data.push(dates[m]);
                new_titles_data.push(titles[m]);
            }
            else{
                var index = new_dates_data.indexOf(dates[m]);
                var tmp_title_data=new_titles_data[index];
                new_titles_data[index]=tmp_title_data+" / "+titles[m];
            }
        }
    }

    var day = new Date(new_dates_data[0]);
    var month = day.getMonth();
    var year = day.getFullYear();

    var firstDay = (new Date(year, month)).getDay(); //o ayın ilk günü belirleniyor.
    var daysInMonth = 32 - new Date(year, month, 32).getDate();//o ay içerisinde kaç gün var belirleniyor.
    
    var tbl = document.getElementById("calendar-body"); // tablo gövdesi
    tbl.style.backgroundColor="#C6E0B4";
    tbl.style.height="100px";

    monthAndYear.innerHTML = months[month] + " " + year; //tablo başlık
    document.getElementById('monthAndYear').style.backgroundColor = '#BDD7EE';
    document.getElementById('monthAndYear').style.textAlign = 'center';

    var date_counter = 1;

    for (var i = 0; i < 6; i++) {

        var row = document.createElement("tr");

        for (var j = 0; j < 7; j++) {
            if (i === 0 && j < firstDay) {
                var cell = document.createElement("td");
                var cellText = document.createTextNode("");
                cell.appendChild(cellText);
                row.appendChild(cell);
            }
            else if (date_counter > daysInMonth) {
                break;
            }
            else {//tablo hücrelerinin oluşturulup , eklenmesi kısmı.
                for(var x=0;x<new_dates_data.length;x++){
                    var cell = document.createElement("td");
                    var date = new Date(new_dates_data[x]);
                                 
                    if (date_counter === date.getDate() && year === date.getFullYear() && month === date.getMonth()){
                        cell.setAttribute("style", "background-color: #4472C4;");
                        cellText = document.createTextNode(date_counter+" "+new_titles_data[x]);
                        cell.appendChild(cellText);   
                        break;
                    }
                    else{
                        cellText = document.createTextNode(date_counter);   
                        cell.appendChild(cellText);
                    }
                }
                row.appendChild(cell);
                tbl.appendChild(row);
                date_counter++;
            }
        }
    }
}